<!-- writeme -->
Drutopia Collection
===================

Drutopia Collection is a feature providing the ability to create collections to bring together ordered listings of Articles, Blog posts, or other content.  This is useful for magazine issues or online books (referencing each chapter) or to create a meta-resource of recommended resources and people. an article content type and related configuration.

 * https://gitlab.com/drutopia/drutopia_collection
 * Package name: drupal/drutopia_collection


### Requirements

 * drupal/config_actions ^1.0
 * drupal/ctools ^3.0
 * drupal/drutopia_people ^1.0-beta3
 * drupal/drutopia_seo ^1.0-beta2
 * drupal/ds ^3.1
 * drupal/metatag ^1.9


### License

GPL-2.0+

<!-- endwriteme -->
